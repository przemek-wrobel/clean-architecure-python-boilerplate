# 3. External Systems

This is the place where we put all **specific** implementations of **Gateways**

# Adapters

implementations of FRONTEND connections (**controllers** and **presenters**)

### Talk inwards with simple structures

## Controllers

for defining processing input

## Presenters

for defining processing output to fronted

# Frameworks

Here goes frameworks with all its **possibilities** and **restrictions**
