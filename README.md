### Clean Architecure – Python Boilerplate

Basic folder structure and procedures to help in Clean Architecture and Test Driven Development

### Libraries

* PyTest https://pytest.org/
* PyLint https://pylint.org
* PyLint Pydantic https://github.com/fcfangcc/pylint-pydantic

Can be useful:

https://github.com/atinfo/awesome-test-automation/blob/master/python-test-automation.md

## Installation

```pip install -r requirements.txt```

## Running PyTest:

```pytest```

## Running PyTest with coverage:

```pytest --cov-report=term:skip-covered --cov-report=html:tests_coverage --cov-branch --cov=app tests```

## Running PyLint:

```pylint --rcfile=.pylintrc --load-plugins pylint_pydantic app tests```

## Running MyPy

```mypy app tests --install-types --disallow-untyped-defs --ignore-missing-imports```

## Running Isort:

```isort app tests --profile black```

## Running Black:

```black -l 120 app tests```
