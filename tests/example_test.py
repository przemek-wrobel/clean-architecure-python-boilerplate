import pytest


def test_without_resource() -> None:
    assert True


@pytest.mark.skip
def skipped_test() -> None:
    assert True


# resource is defined in conftest.py as fixture
def test__that_depends_on_resource(resource: str) -> None:
    assert resource == "resource"
    resource = "changed"
    assert resource == "changed"


def test__that_depends_on_the_same_resource(resource: str) -> None:
    assert resource == "resource"


@pytest.mark.parametrize(
    "data, result",
    [["data", ":)"], ["other data", ":)"]],
)
def test__parametrized(data: str, result: str) -> None:
    assert data
    assert result == ":)"
