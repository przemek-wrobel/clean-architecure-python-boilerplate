import pytest
from _pytest.fixtures import SubRequest


@pytest.fixture(scope="session")
def resource(request: SubRequest) -> str:
    print("resource SETUP")

    def teardown() -> None:
        print("resource TEARDOWN")

    request.addfinalizer(teardown)

    return "resource"
